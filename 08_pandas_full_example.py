# ---
# jupyter:
#   jupytext:
#     cell_metadata_json: true
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Full Example - Dogs and (dog bites) in New York - section 8
# <img src="images/6602794711_eecce6a189_b.jpg">

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Dogs in New York
#
# **Data from New York Open Data (https://opendata.cityofnewyork.us/)**
#
# - Dogs licensing dataset: https://data.cityofnewyork.us/Health/NYC-Dog-Licensing-Dataset/nu7n-tubp
# - Dog bites dataset: https://data.cityofnewyork.us/Health/DOHMH-Dog-Bite-Data/rsgh-akpg
#
# ![newyork_opendata](images/newyork_opendata.png)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Dogs in New York - data import

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Import libraries and dataset

# + {"slideshow": {"slide_type": "-"}, "tags": []}
import os
import pandas
import matplotlib.pyplot

dogs = pandas.read_csv(os.path.join('data', 'NYC_Dog_Licensing_Dataset.csv'),
                       index_col=0,
                       na_values=['Unknown', 'UNKNOWN', 'NAME NOT PROVIDED'])

# + {"slideshow": {"slide_type": "fragment"}}
dogs.head()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Dogs in New York - data cleaning
# -

dogs.rename(columns={
    'BreedName': 'Breed',
    'AnimalGender': 'Gender'},
    inplace=True)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Dogs in New York - data aggregation
# -

# Count number of dogs per breed and gender

dogs_by_breed_and_gender = dogs.groupby(['Breed', 'Gender']).size()
dogs_by_breed_and_gender.head()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Dogs in New York - data reshaping
# -

# Use `unstack` to reshape the table from tall to wide and put female- and male- counts in separate columns.

dogs_by_breed = dogs_by_breed_and_gender.unstack('Gender')
dogs_by_breed.head()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Dogs in New York - data processing
# -

# Let's add another column for total number of dogs per breed

dogs_by_breed['Total'] = dogs_by_breed.sum(axis=1)
dogs_by_breed.head()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Dogs in New York - data processing
# -

# Let's focus on the most common breeds (top 50)

dogs_by_breed = dogs_by_breed.sort_values(by='Total', ascending=False).head(50)
dogs_by_breed.head()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Dogs in New York - data plotting
# -

dogs_by_breed[['F', 'M']].plot.bar(stacked=True, color=['#fa9fb5', '#6baed6'], figsize=(16,6));
matplotlib.pyplot.gca().set_ylabel('Number of dogs');

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Dog bites in New York
# -

# Joining with another dataset

dog_bites = pandas.read_csv(os.path.join('data', 'DOHMH_Dog_Bite_Data.csv'),
                            index_col=0,
                            na_values=['U'])
dog_bites.head()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Dog bites in New York - data aggregation
# -

# Let's group by breed and gender like the previous data-set

dog_bites_by_breed_and_gender = dog_bites.groupby(['Breed', 'Gender']).size()
dog_bites_by_breed_and_gender.head()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Combine dogs info with dig bites info in New York - data join
# -

# Now we can join the two data-sets together, to get both number of dogs and number of bites per breed and gender

# +
bites_per_dog  = dogs_by_breed_and_gender.to_frame('dogs').join(
    dog_bites_by_breed_and_gender.to_frame('bites'), how='outer').\
    sort_values(by='dogs', ascending=False)

bites_per_dog.head()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Dogs in New York - data cleaning
# -

# For some breeds we may not have information about number of dogs or bites.
#     - If there are no known recorded bites for a breed, we replace it with 0.
#     - There may also be cases where we have a recorded bite for a breed where we have no known dog count. This is probably due to breed names not being spelled correctly. We drop these rows.

bites_per_dog[bites_per_dog.isna().any(axis=1)].sample(5)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Dogs in New York - data cleaning
# -

bites_per_dog.dropna(subset=['dogs'], inplace=True)
bites_per_dog.fillna(0, inplace=True)
bites_per_dog.head()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Bites per dog breed in New York
# -

# Now we can caulculate the number of bites per dog for each breed/gender.

bites_per_dog['bites_per_dog'] = bites_per_dog.bites / bites_per_dog.dogs
bites_per_dog.head()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Bites per dog breed in New York
# -

# Lets subset to breeds with many (>5000) data-points

bites_per_dog = bites_per_dog[bites_per_dog.dogs > 5000]
bites_per_dog.head()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Bites per dog breed in New York - safest and most dangerous dog breeds are revelead
# -

ax = bites_per_dog.bites_per_dog.sort_values().plot.bar(figsize=(16,3.5));
ax.set_ylabel('Bites per dog');
ax.set_xticklabels(ax.get_xticklabels(), rotation=20);

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
#   <IMG SRC="images/Black_Labrador_Retriever_portrait.jpg" style="height: 100px; float: left;">
#   <IMG SRC="images/angry-dog-861x618.jpg" style="height: 100px; float: right;">
#
