# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     cell_metadata_json: true
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Pandas import - section 3

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Pandas library
#
# > **pandas is a Python package providing fast, flexible, and expressive data structures** designed to make working with structured (tabular, multidimensional, potentially heterogeneous) and time series data both easy and intuitive**. It aims to be the fundamental high-level building block for doing practical, real world data analysis in Python. Additionally, it has the broader goal of becoming the most powerful and flexible open source data analysis / manipulation tool available in any language. It is already well on its way toward this goal. (From https://pypi.org/project/pandas/)
#
#
# > "the name is derived from the term “panel data”, an econometrics term for multidimensional structured data sets." (Wikipedia)
#
# ![pandas](images/pandas.png)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Pandas library
#
# > pandas is well suited for many different kinds of data:
# > - Tabular data with heterogeneously-typed columns, as in an SQL table or Excel spreadsheet
# - Ordered and unordered (not necessarily fixed-frequency) time series data.
# - Arbitrary matrix data (homogeneously typed or heterogeneous) with row and column labels
# - Any other form of observational / statistical data sets. The data actually need not be labeled at all to be placed into a pandas data structure
#
# > - The two primary data structures of pandas, **Series (1-dimensional) and DataFrame (2-dimensional)**, handle the vast majority of typical use cases in finance, statistics, social science, and many areas of engineering. **For R users, DataFrame provides everything that R’s data.frame provides and much more**. 
# From https://pypi.org/project/pandas/

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Example of data import with pandas

# + {"slideshow": {"slide_type": "-"}}
import os
import pandas

input_filename = os.path.join('data', 'countries.xls') 
countries = pandas.read_excel(input_filename, index_col='Country')
countries

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Step by step

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## Importing libraries

# + {"slideshow": {"slide_type": "-"}}
import os
import pandas

# + [markdown] {"slideshow": {"slide_type": "-"}}
# - Built-in libraries (like "os") is already installed and can just be imported. List of built in modules: 
#   https://docs.python.org/3/py-modindex.html
# - External libraries (like pandas) need to be installed first. Can be installed from the python package index (https://pypi.org/) with the pip command. If you installed python with anaconda (https://www.anaconda.com/), pandas and other data-science packages are aleady installed.
# - Makes library available in the "pandas" namespace. Functions from the pandas library can be called as pandas.(function_name), for example `pandas.read_excel()`
# - Alternative import forms:
#   - `from pandas import read_excel` and `read_excel()`
#   - `from pandas import *` and `read_excel()`
#   - `import pandas as pd` and `pd.read_excel()`

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## Importing data

# + [markdown] {"slideshow": {"slide_type": "-"}}
# ## read_excel
# `pandas.read_excel(io, sheet_name=0, header=0, names=None, index_col=None, usecols=None, squeeze=False, dtype=None, engine=None, converters=None, true_values=None, false_values=None, skiprows=None, nrows=None, na_values=None, keep_default_na=True, verbose=False, parse_dates=False, date_parser=None, thousands=None, comment=None, skip_footer=0, skipfooter=0, convert_float=True, mangle_dupe_cols=True, **kwds)`
#
# - full documentation at https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_excel.html

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## Filename (aka io)

# + {"slideshow": {"slide_type": "-"}}
input_filename = os.path.join('data', 'countries.xls') 
input_filename

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Basic:
# - `countries.xls` - file in the current directory
#
# Sub-directories:
# - `data/countries.xls` - file in the `data` sub-directory (linux/mac)
# - `data\countries.xls` - file in the `data` sub-directory (windows)
# - `os.path.join('data', 'countries.xls')` - file in the `data` sub-directory (any os)
#
# Can also import "directly" from a URL:
# - https://file-examples.com/wp-content/uploads/2017/02/file_example_XLS_10.xls

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ### Setting index/indices

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Basic:
# - `index_col=0` or `index_col='Country'`
#
# Can set multiple indices:
# - for example `index_col=[0,3]` or `index_col=['Country', 'Capital']`
#
# No necessary to set an index, **however** setting index/indices allows for:
# - more performant and simpler look-ups, for example `countries.loc['Ireland']`
# - easier joins between dataframes (will use index/indices by default if what to join on is not specified)
# - separation between metadata (index/indices) and data (regular columns) which makes data manipulation easier (`countries.stack()`, `countries.describe'`, ...)
#
# **Rule of thumbs**: set metadata as index

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## Other pandas imports
#
# - `read_csv`: read from flat file (use separator ',' (default), ';' and '\t' to read different text files)
# - `read_sql`: import data from SQL database
# - `read_spps`: import from SPPS file
# - `read_stata`: import from stata
# - `read_pickle`: read from python object serialization files.
# - ...many more
#
# See full documentation at https://pandas.pydata.org/pandas-docs/stable/reference/io.html

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## Import from MATLAB and R
#
# - No native support from pandas
# - Can be imported with 3rd party packages:
#     - MATLAB .mat files: https://scipy-cookbook.readthedocs.io/items/Reading_mat_files.html
#     - R .rds or .Rdata: https://github.com/ofajardo/pyreadr

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## Building a dataframe directly in pandas

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Make a dictionary with the data to include in the dataframe

# + {"slideshow": {"slide_type": "-"}}
data = {
    'Country': ['Ireland', 'Italy', 'Germany'],
    'Population': [4784000, 60590000, 82790000],
    'Area': [84421, 301338, 357386],
    'Capital': ['Dublin', 'Rome', 'Berlin'],
}

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# Add the data into the dataframe

# + {"slideshow": {"slide_type": "-"}}
countries = pandas.DataFrame(data)
countries

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## Building a dataframe directly in pandas

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Set indices (optional). 
# Note `inplace=True` (alternatively `countries = countries.set_index(['Country', 'Capital'])`)

# + {"slideshow": {"slide_type": "-"}}
countries.set_index(['Country', 'Capital'], inplace=True)
countries

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# ## Pandas export
#
# - `countries.to_excel(filename, ...)`
# - `countries.to_csv(filename, ...)`
# - `countries.to_sql(filename, ...)`
# - `countries.to_stata(filename, ...)`
# - `countries.to_pickle(filename, ...)`

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Alternatives to pandas for heavy datasets
#
# - **Almost** drop-in pandas replacement:
#     - **dask**: https://dask.org/
#     - **datatable**: https://github.com/h2oai/datatable

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Practise
# 1. Go to http://tiny.cc/4c33gz
# 2. Click on "03_pandas_import_practise.py"
# 3. Play around
#
# Continue in 10 min.
