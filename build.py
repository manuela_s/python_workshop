#!/usr/bin/env python
import glob
import os
import pathlib
import subprocess
import sys
import tempfile
import jupytext
import nbconvert.preprocessors


def make_notebook(input, output):
    # Convert to ipynb notebook format and execute notebook.
    notebook = jupytext.read(input)
    preprocessor = nbconvert.preprocessors.ExecutePreprocessor()
    notebook, resources = preprocessor.preprocess(notebook)
    jupytext.write(notebook, output)


def merge_notebooks(notebooks, merged_notebook):
    subprocess.run(['nbmerge', '-o', merged_notebook] + notebooks, check=True)


def convert_to_presentation(notebook, output):
    subprocess.run(['jupyter', 'nbconvert', '--to', 'slides', '--output', output,
                    '--output-dir', '.',
                    '--TagRemovePreprocessor.remove_input_tags={"hide_input"}',
                    notebook], check=True)


def add_html_base_tag(filename, base_url):
    path = pathlib.Path(filename)
    text = path.read_text()
    text = text.replace('<head>', f'<head>\n<base href="{base_url}">', 1)
    path.write_text(text)


def make_presentation(inputs, output):
    """
    Make presentation from multiple .py files

    inputs: list of input files
    output: name of presentation to generate.
    """
    with tempfile.TemporaryDirectory() as tmpdirname:
        notebooks = []
        for input in inputs:
            print(input)
            notebook = os.path.join(tmpdirname, '{}.ipynb'.format(os.path.splitext(input)[0]))
            make_notebook(input, notebook)
            notebooks.append(notebook)

        merged_notebook = os.path.join(tmpdirname, 'merged_notebook.ipynb')
        merge_notebooks(notebooks, merged_notebook)

        convert_to_presentation(merged_notebook, output)
        add_html_base_tag('presentation.slides.html',
                          'https://bitbucket.org/manuela_s/python_workshop/raw/v1/')


if __name__ == '__main__':
    if len(sys.argv) > 1:
        files = sys.argv[1:]
    else:
        files = sorted([ x for x in glob.glob('0?_*.py') if not (
                x.endswith('practise.py') or x.endswith('practise_solutions.py'))])

    make_presentation(files, 'presentation')
