# ---
# jupyter:
#   jupytext:
#     cell_metadata_json: true
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Pandas dataframe foundamentals - section 4

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # The iris dataset
# - The iris dataset (https://en.wikipedia.org/wiki/Iris_flower_data_set) was first described by Fisher in 1936
# - It includes measurements for 4 flower characteristics (length and width for petals and sepals) for 150 flowers from 3 different iris species
#
# ![iris_species](images/iris_species.png)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # The iris dataset
# -

# ![iris_petal_sepal](images/iris_petal_sepal.png) Image from https://www.integratedots.com/determine-number-of-iris-species-with-k-means/

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Let's explore the iris dataset
#
# To load the data, we do:

# + {"slideshow": {"slide_type": "-"}, "tags": ["input_hide"]}
import os
import pandas

# + {"slideshow": {"slide_type": "-"}, "tags": ["input_hide"]}
iris = pandas.read_csv(os.path.join('data', 'iris.csv'))

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Let's explore the iris dataset
# -

# The dataframe has columns with column headers

# + {"tags": ["hide_input"], "slideshow": {"slide_type": "-"}}
iris.head().style.set_table_styles(
    [{'selector': '.col_heading',
      'props': [('background-color', 'yellow')]}]
)

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# Use `iris.columns` to get the column names (returns a pandas Index object)
# -

iris.columns

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Let's explore the iris dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Each column represents 1 variable (i.e. 1 type of attribute/characteristic/feature)

# + {"tags": ["hide_input"]}
iris.head().style.set_table_styles(
    [{'selector': '.col0',
      'props': [('background-color', 'yellow')]}]
)

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# Select a single column by doing `iris.[column_name]` or `iris.column_name` (if no special characters). returns a pandas series object.

# + {"slideshow": {"slide_type": "-"}}
iris['sepal length (cm)']

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Let's explore the iris dataset

# + {"tags": ["hide_input"], "slideshow": {"slide_type": "-"}}
iris.head().style.set_table_styles(
    [{'selector': '.row_heading',
      'props': [('background-color', 'yellow')]}]
)

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# Use `iris.index` to get the index of the table. The index is like a special column that can be used for easy lookups. One of the data-columns can be set to be the index, or by default a simple range-index is used.
# -

iris.index

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Let's explore the iris dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Each row represents 1 observation (i.e. 1 sample/flower)

# + {"tags": ["hide_input"], "slideshow": {"slide_type": "-"}}
iris.head().style.set_table_styles(
    [{'selector': '.row0',
      'props': [('background-color', 'yellow')]}]
)

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# We can select rows based on the row-number (`iris.iloc[row_number]`) or the index (`iris.loc[index_value]`).
# In this case the index value is the same as the row_number (zero-indexed). Returns a pandas series object.
# -

iris.iloc[0]

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Let's explore the iris dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# What is the size of the dataset? rows x columns

# + {"slideshow": {"slide_type": "-"}, "tags": []}
iris.shape

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Let's explore the iris dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Use `iris.head()` to show first few rows (default n=5)

# + {"tags": ["input_hide"], "slideshow": {"slide_type": "-"}}
iris.head()

# + {"slideshow": {"slide_type": "fragment"}}
iris.head(n=3)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Let's explore the iris dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Use `iris.tail()` to show last few rows (default n=5)

# + {"tags": ["input_hide"], "slideshow": {"slide_type": "-"}}
iris.tail()

# + {"tags": ["input_hide"], "slideshow": {"slide_type": "fragment"}}
iris.tail(n=3)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Let's explore the iris dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Use `iris.sample()` to show a random subset of rows (default n=1)

# + {"tags": ["input_hide"], "slideshow": {"slide_type": "-"}}
iris.sample(n=5)

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# Specify `axis='columns'` to show a random subset of columns (deafult n=1)

# + {"tags": ["input_hide"], "slideshow": {"slide_type": "-"}}
iris.sample(n=2).sample(n=3, axis='columns')

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Let's explore the iris dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# `iris.dtypes` to show data type of each column

# + {"tags": ["input_hide"], "slideshow": {"slide_type": "-"}}
iris.dtypes

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Let's explore the iris dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Use `iris.info()` to get an overview of the dataframe

# + {"tags": ["input_hide"], "slideshow": {"slide_type": "-"}}
iris.info()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Practise
# 1. Go to http://tiny.cc/4c33gz
# 2. Click on "04_pandas_dataframe_practise.py"
# 3. Play around
#
# Continue in 15 min.
