# ---
# jupyter:
#   jupytext:
#     cell_metadata_json: true
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Pandas import - practise section 3
#
# ## 10 min
#
# ![kungfu_panda](images/kungfu_panda.jpeg)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Imports

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Import required libraries

# + {"slideshow": {"slide_type": "fragment"}}
import os
import pandas

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # The wine dataset
# - Dataset with chemical analysis of compounds found in grapes from 3 separate cultivars in Italy
# - [Data](https://archive.ics.uci.edu/ml/datasets/Wine) include measurements for 8 compounds
#
# ![mice](images/grapes.jpeg)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # The wine dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Import data from the `'wines.txt'` dataset (in the `'data'` folder) (**Hint**: use `sep='\t'`)

# + {"slideshow": {"slide_type": "fragment"}}
wine = pandas.read_csv(os.path.join('data', 'wine.txt'), sep='\t')
wine.head()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Dataset with reviews for Airbnb in Amsterdam (The Netherlands)

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Import data from `'http://data.insideairbnb.com/the-netherlands/north-holland/amsterdam/2019-09-14/visualisations/listings.csv'`

# + {"slideshow": {"slide_type": "fragment"}}
airbnb = pandas.read_csv('http://data.insideairbnb.com/the-netherlands/north-holland/amsterdam/2019-09-14/visualisations/listings.csv')
airbnb.head(n=2)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # The Boston housing dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Import data from the sheet `regression_dataset` from the `'boston.xlsx'` file included in the folder `'data'` (**hint**: use `sheet_name='regression_dataset'`)

# + {"slideshow": {"slide_type": "fragment"}}
boston = pandas.read_excel(os.path.join('data', 'boston.xlsx'),
                           sheet_name='regression_dataset')
boston.head(n=2)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # The mice protein dataset
#
# - [Data](https://archive.ics.uci.edu/ml/machine-learning-databases/00342/) include expression of 77 proteins from the cerebral cortex of 8 classes of mice (c-CS-s, c-CS-m, c-SC-s, c-SC-m, t-CS-s, t-CS-m, t-SC-s, t-SC-m) from animals with:
#
#     - **2 genotypes**: control (c) & trisonomy (t)
#     - **2 behaviours**: stimulated to learn (control-shock, CS) vs. not-stimulated to learn (shock-control, SC)
#     - **2 treatments**: saline (s) vs. drug memantine (m)
#
# - See https://archive.ics.uci.edu/ml/machine-learning-databases/00342/ for more info
#
# ![mice](images/mice.jpeg)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # The mice protein dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# 1. Import data from `'Data_Cortex_Nuclear.xls'` dataset (in the `'data'` folder) and set the first column as index
# Dataset with reviews for Airbnb in Amsterdam (The Netherlands)
# Import data from 'http://data.insideairbnb.com/the-netherlands/north-holland/amsterdam/2019-09-14/visualisations/listings.csv'
#
# The Boston housing dataset
# Import data from the sheet regression_dataset from the 'boston.xlsx' file included in the folder 'data' (**hint**: use sheet_name='regression_dataset')
#
#
# 2. Set the columns `['Genotype', 'Treatment', 'Behavior', 'class']` as **additional** indices (**hint**: try to pass the flag `append=True` to `set_index()`)

# + {"tags": [], "slideshow": {"slide_type": "fragment"}}
mice = pandas.read_excel(os.path.join('data', 'Data_Cortex_Nuclear.xls'),
                         index_col=0)
mice.set_index(['Genotype', 'Treatment', 'Behavior', 'class'],
               append=True,
               inplace=True)

# + {"slideshow": {"slide_type": "fragment"}}
mice.head(n=2)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # The IMBD dataset
# - Movies metadata from the popular website: https://www.imdb.com/
# - [Data](https://www.kaggle.com/kevalm/movie-imbd-dataset/kernels) include movies metadata such as production year, director, genre, ...
#
# ![imdb_logo](images/imdb.jpeg)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # The IMBD dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# 1. Import data from `'movie-imbd-dataset.zip'` included in the folder `'data'` (**hint**: there is no need to extract the zip file before importing)
# 2. Set the columns `['movie_title', 'movie_imdb_link']` as indices

# + {"slideshow": {"slide_type": "fragment"}, "tags": []}
movies = pandas.read_csv(os.path.join('data', 'movie-imbd-dataset.zip'),
                         index_col=['movie_title', 'movie_imdb_link'])

movies.head(n=2)
