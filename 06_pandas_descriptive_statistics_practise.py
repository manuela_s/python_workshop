# ---
# jupyter:
#   jupytext:
#     cell_metadata_json: true
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Pandas dataframe foundamentals - practise section 6
#
# ## 10 min
#
# ![kungfu_panda](images/kungfu_panda.jpeg)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# #  Let's import the IMBD dataset
#
# 1. Add required import statements to import data
# 2. Import data from `'movie-imbd-dataset.zip'` included in the folder `'data'`
# 2. Set the columns `['movie_title', 'movie_imdb_link']` as indices

# + {"slideshow": {"slide_type": "-"}, "tags": []}
import os
import pandas

movies = pandas.read_csv(os.path.join('data', 'movie-imbd-dataset.zip'),
                         index_col=['movie_title', 'movie_imdb_link'])

movies.head(n=2)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # What is the average length of a movie?

# + {"slideshow": {"slide_type": "fragment"}}


# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Which director has directed most movies?

# + {"slideshow": {"slide_type": "fragment"}}


# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # What are the average lenght of movies directed by top 10 directors?

# + {"slideshow": {"slide_type": "fragment"}}


# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Has the number of movies produced by year grown over time?

# + [markdown] {"slideshow": {"slide_type": "-"}}
# 1. Identify columns to look at
# 2. Get breakdown for number of movies per year
# 3. Plot

# + {"slideshow": {"slide_type": "fragment"}}


# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Do movies in English have in average more likes on Facebook than movies in French or Spanish?

# + [markdown] {"slideshow": {"slide_type": "-"}}
# 1. Identify columns to look at
# 2. Compute average (you choose which type) number of likes for English, French and Spanish
# 3. Let the number speak!

# + {"slideshow": {"slide_type": "fragment"}, "tags": []}

