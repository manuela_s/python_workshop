# ---
# jupyter:
#   jupytext:
#     cell_metadata_json: true
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Jupyter notebook - practise section 2
# ## 5 min
#
# ![jupyter_notebook](images/jupyter_notebook_logo.png)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# Simple expressions.
# Use the "Run" icon (or press ctrl-enter) to run each cell.
#
# **Hint**: Try + - / and *

# + {"slideshow": {"slide_type": "-"}}
2+2

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# Storing results in variables

# + {"slideshow": {"slide_type": "-"}}
a = 2 + 2
b = 1 + 1
a + b

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# Cells can reference variables from previous cells (make sure to run the previous cells first!):

# + {"slideshow": {"slide_type": "-"}}
a - b
# -

a

a=-2
a-b

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# Use the Insert cell menu to insert another code cell.
