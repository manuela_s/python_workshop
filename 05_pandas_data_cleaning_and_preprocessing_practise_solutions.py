# ---
# jupyter:
#   jupytext:
#     cell_metadata_json: true
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Pandas dataframe foundamentals - practise section 5
#
# ## 15 min
#
# ![kungfu_panda](images/kungfu_panda.jpeg)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# #  Let's import the JSE Ok Cupid dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# 1. Add required import statements to import data
# 2. Import dataset stored in `'profiles.csv'` in the folder `'data'`
# 3. Drop from the dataset **only** columns that start with "essay"
# 4. Drop from the dataset columns with missing data for more than half of the observations

# + {"slideshow": {"slide_type": "-"}, "tags": []}
import os
import pandas

data = pandas.read_csv(os.path.join('data', 'profiles.csv'))
data.drop(columns=data.columns[data.columns.str.startswith('essay')], inplace=True)
data.drop(columns=data.columns[(data.isna().mean() > 0.5)], inplace=True)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Let's synthesize new columns by extracting info from existing columns

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Check content of the column `'location'`

# + {"slideshow": {"slide_type": "fragment"}, "tags": []}
data.location.value_counts()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Where are users looking for love from?

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Create 2 new columns `'city'` and `'state'` by extracting the city and state information from the column `'location'` (**Hint**: use `.str.extract()`)

# + {"slideshow": {"slide_type": "fragment"}, "tags": []}
data['city'] = data.location.str.extract('(.*),')
data['state'] = data.location.str.extract(', (.*)')

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Where are users looking for love from?

# + [markdown] {"slideshow": {"slide_type": "-"}}
# 1. Get a breakdown of where users are from (state)
# 2. List top 3 states with most users

# + {"slideshow": {"slide_type": "fragment"}, "tags": []}
data.state.value_counts(normalize=True).head(3)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Let's focus our search for love to California

# + [markdown] {"slideshow": {"slide_type": "-"}}
# 1. Subset dataset to only include users from California
# 2. Plot number of users from top 20 cities in California (**hint**: use`.plot(kind='bar')` method on dataframe)

# + {"slideshow": {"slide_type": "fragment"}, "tags": []}
data = data[data.state=='california']
data.city.value_counts().head(20).plot(kind='barh', figsize=(16, 6))

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Let's synthesize new columns by extracting info from existing columns

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Check content of the column `'sign'` (astrological sign)

# + {"tags": [], "slideshow": {"slide_type": "fragment"}}
data.sign.value_counts()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Let's synthesize new columns by extracting info from existing columns

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Create a new column `'sign_type'` including **only** the astrological sign (**Hint**: use `.str.extract()`)

# + {"tags": [], "slideshow": {"slide_type": "fragment"}}
data['sign_type'] = data.sign.str.extract('([\w]+)')
data['sign_type'].value_counts()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Let's synthesize new columns by extracting info from existing columns

# + [markdown] {"slideshow": {"slide_type": "-"}}
# 1. Create a new column `'sign_importance'` with 3 categories (`'fun'`, `'irrelevant'`, `'important'`) (**Hint**: use `.str.extract(..., regex=True)`)
# 2. Keep **only** entries for users that do not think astrological sign is important

# + {"tags": [], "slideshow": {"slide_type": "fragment"}}
data['sign_importance'] = data.sign.str.extract('[\w]+ (.*)', expand=False).replace({
    '.*fun to think about': 'fun',
    '.*doesn&rsquo;t matter': 'indifferent',
    '.*matters a lot': 'important'}, regex=True)
data['sign_importance'].value_counts()
data = data[data.sign_importance != 'important'].copy()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Let's spend some time working at Berkeley university...& search for love

# + [markdown] {"slideshow": {"slide_type": "-"}}
# 1. Subset dataset to only include users that:
#     - are from Berkeley
#     - are males
#     - are single
#     - aged between 25 and 40 (extremes included)
#     - never do drugs
#     - do not smoke
#     - do not drink often
#     - eat anything (and variations thereof) (**hint**: use `.str.contains()`)
#     - over 1.75 m in height (**hint**: convert by multiplying by 0.0254)
#     - speak Italian (**hint**: use `.str.contains()`)
#     - do not have dogs (**hint**: use `.str.contains()`)
# 2. Count how many potential candidate partners there are for us that meet these criteria

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Let's spend some time working at Berkeley university...& search for love

# + {"slideshow": {"slide_type": "-"}, "tags": []}
# Extract infor required for subsetting and store it in columns
data['height_m'] = data.height * 0.0254 # Convert height from inch to m
data['has_dogs'] = data.pets.str.contains('has dogs')
data['speaks_italian'] = data.speaks.str.contains('italian')
# data['num_spoken_languages'] = data.speaks.str.split(',').apply(len)

# Subset to candidates
candidates = data[
    (data.city=='berkeley') &
    (data.sex=='m') &
    (data.status=='single') &
    (data.age >=25) & (data.age <= 40) &
    (data.drugs=='never') &
    (data.smokes=='no') &
    (data.drinks!='often') &
    (data.diet.str.contains('anything')) &
    (data.height_m>1.75) &
    (data.speaks_italian) &
    (data.has_dogs!=True)]

candidates.shape[0]
