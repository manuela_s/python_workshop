# ---
# jupyter:
#   jupytext:
#     cell_metadata_json: true
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Pandas dataframe foundamentals - practise section 4
#
# ## 15 min
#
# ![kungfu_panda](images/kungfu_panda.jpeg)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # The JSE Ok Cupid dataset
# - Online dating data from the popular website https://www.okcupid.com/
# - [Data](https://github.com/rudeboybert/JSE_OkCupid) from **"OkCupid data for introductory statistics and data science courses"** by Kim and Escobedo-Land (Journal of Statistics Education July 2015, Volume 23, Number 2)
#
# ![ok_cupid_logo](images/ok_cupid_logo.png)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# #  Let's import the JSE Ok Cupid dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# 1. Add required import statements to import data
# 2. Import dataset stored in `'profiles.csv'` in the folder `'data'`

# + {"slideshow": {"slide_type": "fragment"}, "tags": []}


# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Let's explore the JSE Ok Cupid dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Show data for the top 3 entries

# + {"tags": [], "slideshow": {"slide_type": "fragment"}}


# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Let's explore the JSE Ok Cupid dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Get dataset dimensions (number of rows and columns)

# + {"tags": [], "slideshow": {"slide_type": "fragment"}}


# + [markdown] {"slideshow": {"slide_type": "-"}}
# Get column headers to check what info is included

# + {"slideshow": {"slide_type": "fragment"}}


# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Let's explore the JSE Ok Cupid dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Get a glimpse of the dataset as a whole (**hint**: `.info()`)

# + {"tags": [], "slideshow": {"slide_type": "fragment"}}


# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Let's explore the "essay" columns of the JSE Ok Cupid dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Show top 3 entries and include **only** columns that start with "essay" (**hint**: `data.columns.str.startswith()`)

# + {"tags": [], "slideshow": {"slide_type": "fragment"}}


# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Drop "essay" columns of the JSE Ok Cupid dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Drop from the dataset **only** columns that start with "essay" (**hint**: `data.columns.str.startswith()`)

# + {"tags": [], "slideshow": {"slide_type": "fragment"}}


# + {"tags": [], "slideshow": {"slide_type": "fragment"}}


# + {"slideshow": {"slide_type": "fragment"}}


# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Missing data in the JSE Ok Cupid dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# How many missing data are there for each column (**hint 1**: `data.isna()`, **hint 2**: `True` and `False` evaluate to `0` and `1`, respectively)

# + {"tags": [], "slideshow": {"slide_type": "fragment"}}


# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Missing data in the JSE Ok Cupid dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Express missing data as fraction (or percentage) of total number of observations and show **only top 5** columns sorted by missingness (**hint**: `sort_values`)

# + {"tags": [], "slideshow": {"slide_type": "fragment"}}


# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Missing data in the JSE Ok Cupid dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# How many missing data are there for each observation (**hint**: specify `axis`)

# + {"tags": [], "slideshow": {"slide_type": "fragment"}}


# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Missing data in the JSE Ok Cupid dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Drop from the dataset columns with missing data for more than half of the observations

# + {"tags": [], "slideshow": {"slide_type": "fragment"}}


# + [markdown] {"slideshow": {"slide_type": "-"}}
# Verify column is gone from dataset

# + {"tags": [], "slideshow": {"slide_type": "fragment"}}


# + {"tags": [], "slideshow": {"slide_type": "fragment"}}

