# ---
# jupyter:
#   jupytext:
#     cell_metadata_json: true
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Data cleaning and preprocessing with pandas - section 5

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Data cleaning and preprocessing with pandas
# To load the data, we do:

# + {"slideshow": {"slide_type": "-"}, "tags": ["input_hide"]}
import os
import numpy
import pandas

# + {"slideshow": {"slide_type": "-"}, "tags": ["input_hide"]}
iris = pandas.read_csv(os.path.join('data', 'iris.csv'))

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Data cleaning and preprocessing
# -

# The iris dataset is a "clean" dataset, but real-world datasets never are...
#
# Common data cleaning includes:
# - recode missing data
# - harmonize entries (spelling, etc.)
# - dropping columns (redundant, not informative, etc.)
# - drop rows (duplicates, missing data, etc.)
# - map original values to values required for downstream analysis (binarize, convert units, etc.) 
# - synthetizing features (features engineering)
# - normalization/scaling features

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Let's make the iris dataset a bit messy...

# +
# Change the spelling of the species on some of the rows
to_replace = iris.species.sample(frac=0.2, random_state=2)
iris.loc[to_replace.index, 'species'] = to_replace.str.upper()

# Replace some of the values with NaN (missing value)
iris.where(numpy.random.RandomState(seed=0).random(iris.shape) > 0.05, numpy.nan, inplace=True)

# + {"slideshow": {"slide_type": "fragment"}}
iris.head()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Missing data
# -

# Use `iris.isna()` to visualise missing data (`True` if missing)

iris.isna().head()

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# Use `iris.isna().sum()` to count missing data along each column
# -

iris.isna().sum()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Missing data
# -

# Use `iris.isna().sum(axis=1)` to count missing data along each row

iris.isna().sum(axis=1).head()

# Use `iris.isna().all().sum(axis=1)` to count rows with missing data across all columns

iris.isna().all(axis=1).sum()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Missing data
# -

# Use `iris.dropna()` to drop rows with missing data (by default a row is dropped if contains 'any' missing data)

iris.dropna().shape

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# Use `iris.dropna(how='all')` to drop rows with missing data in *all* columns
# -

iris.dropna(how='all').shape

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# Use `iris.dropna(subset=[column names])` to drop rows with missing data in specific columns (list of column names)
# -

iris.dropna(subset=['sepal length (cm)', 'sepal width (cm)']).shape

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Filling missing data
#

# + [markdown] {"slideshow": {"slide_type": "-"}}
# **Important note**: whether or not to fill missing data and how depends on the dataset and the specific research question

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Replace missing data with constant value (for example 0, for illustration only)

# + {"slideshow": {"slide_type": "-"}}
iris.fillna(0).head()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Filling missing data
#

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Replace missing data with constant value per column (for example median, for illustration only)

# + {"slideshow": {"slide_type": "-"}}
iris.fillna(iris.median()).head()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Filling missing data
#

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Replace missing data by linear interpolation (for illustration only)

# + {"slideshow": {"slide_type": "-"}}
iris.interpolate(method='linear').head()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Harmonize entries

# + [markdown] {"slideshow": {"slide_type": "-"}}
# See existing spellings

# + {"slideshow": {"slide_type": "-"}}
iris.species.value_counts()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Harmonize entries

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Use `.map()` with a dictionary that maps current to desidered values

# + {"slideshow": {"slide_type": "-"}}
iris.species.map({
    'virginica': 'Virginica',
    'versicolor': 'Versicolor',
    'setosa': 'Setosa',
    'VIRGINICA': 'Virginica',
    'VERSICOLOR': 'Versicolor',
    'SETOSA': 'Setosa',
}).value_counts()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Harmonize entries

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Use `.replace()` with a dictionary that maps current to desidered values. Unlisted values remain unchanged
# -

iris.species.replace({
    'VIRGINICA': 'virginica',
    'VERSICOLOR': 'versicolor',
    'SETOSA': 'setosa',
}).value_counts()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Harmonize entries

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Use `.replace()` with a dictionary that maps current to desidered values using regular expression (https://en.wikipedia.org/wiki/Regular_expression)
# -

iris.species.replace({
    'VIRGINICA|virginica': 'Virginica', # A or B
    '(?i:VERSICOLOR)': 'Versicolor', # Case insensitive groups
    '[Ss].*': 'Setosa', # String starting with S or S
}, regex=True).value_counts()


# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Harmonize entries

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Use your own function for custom logic
# -

def harmonize_species(old_name):
    if old_name == 'SETOSA':
        return 'setosa'
    elif old_name == 'VIRGINICA':
        return 'virginica'
    elif old_name == 'VERSICOLOR':
        return 'versicolor'
    else:
        return old_name
iris.species.apply(harmonize_species).value_counts()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Harmonize entries

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Use pandas built-in string methods for common manipulations (https://pandas.pydata.org/pandas-docs/stable/user_guide/text.html#method-summary)
# -

iris.species.str.title().value_counts()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Removing data

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Use `iris.drop(index=index_to_drop)` to drop rows with specific index/indices
# -

iris.drop(index=range(0,5)).head()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Removing data

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Use `iris.drop(columns=columns_to_drop)` to drop columns with specific column(s)
# -

iris.drop(columns=['sepal length (cm)', 'sepal width (cm)']).head()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Removing data

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Use `.drop_duplicated()` to drop rows with duplicated values
# -

iris.drop_duplicates().shape

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Discretize numeric data

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Use `pandas.qcut()` to discretize in quantiles
# -

pandas.qcut(iris['sepal length (cm)'], q=3).value_counts(sort=False)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Discretize numeric data

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Use `pandas.cut()` to discretize in n equally wide bins
# -

pandas.cut(iris['sepal length (cm)'], bins=3).value_counts(sort=False)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Discretize numeric data

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Use `pandas.cut()` to discretize with a custom bin edges (for example, [0, mean, Inf))
# -

pandas.cut(iris['sepal length (cm)'], bins=[0, iris['sepal length (cm)'].mean(), numpy.Inf]).value_counts(sort=False)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Unit conversion

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Use standard arithmetic operations to scale values in columns (for example, convert measurements to meters)
# -

iris['sepal length (cm)'].head() / 100

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Synthetizing new columns by combining existing columns

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Use standard arithmetic operations to construct a new column (for example, compute ratio length to width for sepal)
# -

(iris['sepal length (cm)'] / iris['sepal width (cm)']).head()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Putting all together

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Drop rows with missing values and save result into a new dataframe called `iris_clean`

# + {"slideshow": {"slide_type": "-"}}
iris_clean = iris.dropna().copy()

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# Harmonize species spelling and save results back in the species column

# + {"slideshow": {"slide_type": "-"}}
iris_clean['species'] = iris_clean['species'].str.title()

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# Synthetize new column with ratio between length and width for sepals and petals

# + {"slideshow": {"slide_type": "-"}}
iris_clean['sepal_length_over_width_ratio'] = (iris_clean['sepal length (cm)'] / iris_clean['sepal width (cm)'])
iris_clean['petal_length_over_width_ratio'] = (iris_clean['petal length (cm)'] / iris_clean['petal width (cm)'])

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# Drop redundant columns

# + {"slideshow": {"slide_type": "-"}}
iris_clean = iris_clean.drop(columns=iris.columns[0:4])

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Resulting dataframe

# + {"slideshow": {"slide_type": "-"}}
iris_clean.head()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Practise
# 1. Go to http://tiny.cc/4c33gz
# 2. Click on "05_pandas_data_cleaning_and_preprocessing_practise.py"
# 3. Play around
#
# Continue in 15 min.
