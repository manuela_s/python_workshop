# ---
# jupyter:
#   jupytext:
#     cell_metadata_json: true
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Data plotting - section 07

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Data plotting
#
# - Exploratory data analysis (EDA) is a crucial part of any research project
#     - quality control (missing data, outlier detection, ...)
#     - descriptive statistics
#     - visualization
#     - insights to guide downstream analysis
#
# ![data_exploration_cartoon](images/data_exploration_cartoon.png) Image from http://www.codeheroku.com/post.html?name=Introduction%20to%20Exploratory%20Data%20Analysis%20(EDA)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Beyond built-in plotting with pandas
#
# (A few) of python's plotting libraries:
# - **matplotlib**: https://matplotlib.org/
# - **seaborn**: https://seaborn.pydata.org/
# - plotnine (ggplot-like): https://plotnine.readthedocs.io/en/stable/
# - plotly: https://plot.ly/
# - bokeh: https://docs.bokeh.org/en/latest/index.html
# - ...

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Matplotlib
#
# > **Matplotlib is a Python 2D plotting library which produces publication quality figures in a variety of hardcopy formats and interactive environments across platforms**.
#
#
# > **Matplotlib tries to make easy things easy and hard things possible**. You can generate plots, histograms, power spectra, bar charts, errorcharts, scatterplots, etc., with just a few lines of code. For examples, see the sample plots and thumbnail gallery.
#
# > For the power user, you have full control of line styles, font properties, axes properties, etc, via an object oriented interface or via a set of functions familiar to MATLAB users.
#
# From https://matplotlib.org/

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Seaborn
#
# > Seaborn is a Python visualization library based on matplotlib. It provides a high-level interface for drawing attractive statistical graphics. (https://seaborn.pydata.org/index.html)
#
#
# - Examples gallery: https://seaborn.pydata.org/examples/index.html
#
# <div class="row">
#
# <a href=https://seaborn.pydata.org/examples/many_facets.html>
# <img src="https://seaborn.pydata.org/_static/many_facets_thumb.png" height="135" width="135">
# </a>
#
# <a href=https://seaborn.pydata.org/examples/horizontal_boxplot.html>
# <img src="https://seaborn.pydata.org/_static/horizontal_boxplot_thumb.png" height="135" width="135">
# </a>
#
# </div>
#

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Let's get plotting the iris dataset

# + {"slideshow": {"slide_type": "-"}, "tags": ["input_hide"]}
import os
import numpy
import pandas

import matplotlib.pyplot
import seaborn

iris = pandas.read_csv(os.path.join('data', 'iris.csv'))

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Univariate analysis - swarm-plot
# -

seaborn.swarmplot(y='sepal length (cm)', color='k', data=iris);

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Grouped swarm-plot
# -

seaborn.swarmplot(x='species', y='sepal length (cm)', data=iris.reset_index());

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Grouped box-plot
# -

seaborn.boxplot(x='species', y='sepal length (cm)', data=iris.reset_index());

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Grouped violin-plot

# + {"slideshow": {"slide_type": "-"}}
seaborn.violinplot(x='species', y='sepal length (cm)', data=iris.reset_index());

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# Combining multiple univariate plots
# -

seaborn.violinplot(x='species', y='sepal length (cm)', data=iris.reset_index());
seaborn.swarmplot(x='species', y='sepal length (cm)', color='w', data=iris.reset_index());

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Compose a figure with multiple sub-plots

# + {"slideshow": {"slide_type": "-"}}
fig, axes = matplotlib.pyplot.subplots(nrows=1, ncols=4, figsize=(24, 8))
# Dot plot with no grouping variable
seaborn.swarmplot(y='sepal length (cm)', color='k', data=iris, ax=axes[0])
# Dot plot grouped by species
seaborn.swarmplot(x='species', y='sepal length (cm)', dodge=True, data=iris.reset_index(), ax=axes[1])
# Boxplot grouped by species
seaborn.boxplot(x='species', y='sepal length (cm)', dodge=True, data=iris.reset_index(), ax=axes[2])
# Violin plot grouped by species
seaborn.violinplot(x='species', y='sepal length (cm)', dodge=True, data=iris.reset_index(), ax=axes[3]);

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Compose a figure with a grid of *identical* sub-plots (FacetGrid)
# -

# [seaborn.catplot](https://seaborn.pydata.org/generated/seaborn.catplot.html)
# > Figure-level interface for drawing categorical plots onto a FacetGrid.
#
# [seaborn.relplot](https://seaborn.pydata.org/generated/seaborn.relplot.html)
# > Figure-level interface for drawing relational plots onto a FacetGrid.
#
# > x, y, hue : names of variables in data
# > Inputs for plotting **long-form data**. See examples for interpretation.

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Reshape a dataframe from a wide-format to a long-format
# -

# Wide-format:

# + {"slideshow": {"slide_type": "-"}}
iris.loc[[6, 92, 140]]

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Reshape a dataframe from a wide-format to a long-format

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Let's stack the table and add a grouping variable to convert to a tall format

# + {"slideshow": {"slide_type": "-"}}
iris_long = iris.set_index('species', append=True).stack().to_frame('value')
iris_long.index.names = ['id', 'species', 'feature']
iris_long.loc[[6, 92, 140]]

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Draw a grid of sub-plots where each categorical plot includes a single feature

# + {"slideshow": {"slide_type": "-"}}
seaborn.catplot(x='species', y='value', col='feature', sharey=False, data=iris_long.reset_index());

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Bivariate analysis - scatter-plot

# + {"slideshow": {"slide_type": "-"}}
seaborn.scatterplot(x='sepal length (cm)', y='petal width (cm)', hue='species', data=iris);

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Bivariate analysis - scatterplot + histograms (joint-plot)

# + {"slideshow": {"slide_type": "-"}}
seaborn.jointplot(x='sepal length (cm)', y='petal width (cm)', kind='reg', data=iris);

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Draw a grid of sub-plots where each relational plot includes a single feature

# + {"slideshow": {"slide_type": "-"}}
seaborn.relplot(x='sepal length (cm)', y='petal width (cm)', col='species', data=iris.reset_index());

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Visualize relationship and distribution between feature pairs

# + {"slideshow": {"slide_type": "-"}}
seaborn.pairplot(iris, hue='species');

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Plot heatmap to visualize correlation among features

# + {"slideshow": {"slide_type": "-"}, "tags": ["hide_input"]}
corr = iris.drop(columns=['species']).corr()
mask = numpy.zeros_like(corr)
mask[numpy.triu_indices_from(mask)] = True
seaborn.heatmap(corr,
                mask=mask,
                square=True,
                cmap='RdBu',
                vmin=-1,
                vmax=1,
                linewidth=1);

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Practise
# 1. Go to http://tiny.cc/4c33gz
# 2. Click on "07_data_plotting_practise.py"
# 3. Play around
#
# Continue in 10 min.
