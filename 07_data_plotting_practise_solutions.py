# ---
# jupyter:
#   jupytext:
#     cell_metadata_json: true
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Pandas dataframe foundamentals - practise section 7
#
# ## 10 min
#
# ![kungfu_panda](images/kungfu_panda.jpeg)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# #  Let's import the IMBD dataset
#
# 1. Add required import statements to import data
# 2. Import data from `'movie-imbd-dataset.zip'` included in the folder `'data'`
# 2. Set the columns `['movie_title', 'movie_imdb_link']` as indices

# + {"slideshow": {"slide_type": "-"}, "tags": []}
import os
import pandas

import matplotlib.pyplot
import seaborn

movies = pandas.read_csv(os.path.join('data', 'movie-imbd-dataset.zip'),
                         index_col=['movie_title', 'movie_imdb_link'])

movies.head(n=2)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Does the number of Facebook likes that movies receive correlate with the IMDB score?

# + [markdown] {"slideshow": {"slide_type": "-"}}
# 1. Identify 2 relevant variables to look at
# 2. Scatter the 2 variables and add a regression line (**hint**: use [`seaborn.lmplot()`](https://seaborn.pydata.org/generated/seaborn.lmplot.html)
# 3. Compute correlation between the 2 variables (**hint**: use dataframe.(columns1).corr(dataframe.column2)
# 4. Print result in the title (**hint**: use `matplotlib.pyplot.title()` to s.et the title

# + {"slideshow": {"slide_type": "fragment"}}
seaborn.lmplot(x='movie_facebook_likes', y='imdb_score',
                data=movies)
matplotlib.pyplot.title(
    'Spearman correlation={:.2f}'.format(
        movies.movie_facebook_likes.corr(movies.imdb_score, method='spearman')));

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Does the number of Facebook likes that movies receive correlate with the IMDB score?

# + [markdown] {"slideshow": {"slide_type": "-"}}
# 1. Subset dataset to only include movies in English, French and Spanish
# 2. Redo scatterplotas above, but customise:
#     - color-code by language
#         - use 'Set2' for colors (**hint**: set `palette='Set2')
#         - do not show shading for regression line (**hint**: set `ci=None`
#         - increase marker size

# + {"slideshow": {"slide_type": "fragment"}}
movies_subset = movies[movies.language.isin(['English', 'French', 'Spanish'])]
seaborn.lmplot(x='movie_facebook_likes', y='imdb_score',
               hue='language',
               palette='Set2',
               ci=None,
               scatter_kws={"s": 100},
               data=movies_subset);

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Let's import the mice protein expression dataset

# + {"slideshow": {"slide_type": "-"}, "tags": []}
mice = pandas.read_excel(os.path.join('data', 'Data_Cortex_Nuclear.xls'),
                         index_col=0)
mice.set_index(['Genotype', 'Treatment', 'Behavior', 'class'],
               append=True,
               inplace=True)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Visualize expression of the protein DYRK1A_N by genotype
# -

# 1. Use [`seaborn.swarmplot()`](https://seaborn.pydata.org/generated/seaborn.swarmplot.html) to visualize protein expression (**hint**: set `x=Genotype` and `reset_index()` on the dataframe)

# + {"slideshow": {"slide_type": "fragment"}, "tags": []}
seaborn.swarmplot(x='Genotype', y='DYRK1A_N', data=mice.reset_index());

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Visualize expression of the protein DYRK1A_N by genotype & treatment
# -

# 1. Use [`seaborn.swarmplot()`](https://seaborn.pydata.org/generated/seaborn.swarmplot.html) to visualize protein expression (**hint**: set `hue=Treatment`)

# + {"slideshow": {"slide_type": "fragment"}, "tags": []}
seaborn.swarmplot(x='Genotype', y='DYRK1A_N',
                  hue='Treatment',
                  dodge=True,
                  data=mice.reset_index());

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Visualize expression of the protein DYRK1A_N by genotype & treatment & behavior
# -

# 1. Use [`seaborn.swarmplot()`](https://seaborn.pydata.org/generated/seaborn.swarmplot.html) to visualize protein expression (**hint**: set `hue=Treatment`)

# + {"slideshow": {"slide_type": "fragment"}, "tags": []}
seaborn.catplot(x='Genotype', y='DYRK1A_N',
                hue='Treatment',
                dodge=True,
                col='Behavior',
                data=mice.reset_index());

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Visualize protein expression
# -

# 1. Use [`seaborn.heatmap()`](https://seaborn.pydata.org/generated/seaborn.heatmap.html) to visualize protein expression

# + {"slideshow": {"slide_type": "fragment"}, "tags": []}
ax = seaborn.heatmap(mice,
                cmap='Blues',
                xticklabels=mice.columns)
ax.figure.set_figwidth(16)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Cluster protein expression
# -

# 1. Use [`seaborn.clustermap()`](https://seaborn.pydata.org/generated/seaborn.clustermap.html?highlight=clustermap#seaborn.clustermap) to cluster protein expression
# 2. Consider whether or not scaling is required (**hint**: set `standard_scale`)

# + {"slideshow": {"slide_type": "fragment"}, "tags": []}
mice_no_na = mice.dropna()
seaborn.clustermap(mice_no_na,
                   cmap='Blues',
                   standard_scale=1,
                   xticklabels=mice_no_na.columns,
                   row_colors=matplotlib.cm.Set1(mice_no_na.reset_index()['class'].astype('category').cat.codes));

