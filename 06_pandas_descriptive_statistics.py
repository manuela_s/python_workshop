# ---
# jupyter:
#   jupytext:
#     cell_metadata_json: true
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Descriptive statistics with pandas - section 6

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Descriptive statistics for the iris dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Import libraries and dataset

# + {"slideshow": {"slide_type": "-"}, "tags": []}
import os
import pandas
import numpy

iris = pandas.read_csv(os.path.join('data', 'iris.csv'))
iris.head()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Descriptive statistics for the iris dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Use `iris.mean()` to get mean across all rows for each numeric column

# + {"slideshow": {"slide_type": "-"}, "tags": []}
iris.mean()

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Use `iris.mean(axis=1)` to get mean across all numeric columns for each row

# + {"slideshow": {"slide_type": "-"}, "tags": []}
iris.mean(axis=1)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Descriptive statistics for the iris dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Use `iris.median()` to get median across all rows for each numeric column

# + {"slideshow": {"slide_type": "-"}, "tags": []}
iris.median()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Descriptive statistics for the iris dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Similar common aggregation functions are available:
#
#     - `.sum()`
#     - `.quantile()`
#     - `.count()`
#     - `.min()`
#     - `.max()`
#     - `.std()`
#     - `.abs()`
#     - `.corr()`
#     - ....
#
# Check documentation at https://pandas.pydata.org/pandas-docs/stable/reference/frame.html#computations-descriptive-stats

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# Can be applied to full dataframe or to selected column(s)

# + {"slideshow": {"slide_type": "-"}, "tags": []}
iris['sepal length (cm)'].quantile(0.5)

# + {"slideshow": {"slide_type": "-"}, "tags": []}
iris[['sepal length (cm)', 'sepal width (cm)']].quantile(0.5)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Descriptive statistics for the iris dataset

# + {"slideshow": {"slide_type": "-"}, "tags": []}
# Compute count
iris['sepal length (cm)'].count()

# + {"slideshow": {"slide_type": "fragment"}, "tags": []}
# Compute min
iris['sepal length (cm)'].min()

# + {"slideshow": {"slide_type": "fragment"}, "tags": []}
# Compute max
iris['sepal length (cm)'].max()

# + {"slideshow": {"slide_type": "fragment"}, "tags": []}
# Compute standard deviation
iris['sepal length (cm)'].std()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Descriptive statistics for the iris dataset

# + {"slideshow": {"slide_type": "-"}, "tags": []}
# Compute abs
iris['sepal length (cm)'].abs()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Descriptive statistics for the iris dataset

# + {"slideshow": {"slide_type": "-"}, "tags": []}
# Compute correlation (by deafult uses `method='pearson'`)
iris.corr()

# + {"slideshow": {"slide_type": "-"}, "tags": []}
# Compute Spearman correlation
iris.corr(method='spearman')

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Descriptive statistics for the iris dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Other useful functions:
#     - `.nsmallest`
#     - `.nlargest`
#     - `.idxmin()`
#     - `.idxmax()`

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# - `.nlargest()` and `.nsmallest()` are applied to a specific column
# - `.idxmax()` and `.idxmin()` can be applied to either a full dataframe or to selected column(s)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Descriptive statistics for the iris dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Use `iris.nsmallest()` to identify rows with highest measurement for a column

# + {"slideshow": {"slide_type": "-"}, "tags": []}
iris.nsmallest(n=3, columns='petal length (cm)')

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Verify results

# + {"slideshow": {"slide_type": "-"}, "tags": []}
iris['petal length (cm)'].min()

# + {"slideshow": {"slide_type": "-"}, "tags": []}
iris['petal length (cm)'].idxmin()

# + {"slideshow": {"slide_type": "-"}, "tags": []}
iris['petal length (cm)'].sort_values().head(3)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Descriptive statistics for the iris dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Use `iris.nlargest()` to identify rows with highest measurement for a column

# + {"slideshow": {"slide_type": "-"}, "tags": []}
iris.nlargest(n=3, columns='petal length (cm)')

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Verify results

# + {"slideshow": {"slide_type": "-"}, "tags": []}
iris['petal length (cm)'].max()

# + {"slideshow": {"slide_type": "-"}, "tags": []}
iris['petal length (cm)'].idxmax()

# + {"slideshow": {"slide_type": "-"}, "tags": []}
iris['petal length (cm)'].sort_values(ascending=False).head(3)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Descriptive statistics for the iris dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Use `iris.apply()` to apply a custom function. Note: subset to numerical columns

# + {"slideshow": {"slide_type": "-"}, "tags": []}
iris.iloc[:, 0:4].head()

# + {"slideshow": {"slide_type": "-"}, "tags": []}
iris.iloc[:, 0:4].apply(numpy.mean)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Descriptive statistics for the iris dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Use `iris.aggregate()` to apply one or more custom functions

# + {"slideshow": {"slide_type": "-"}, "tags": []}
iris.iloc[:, 0:4].aggregate(lambda x: numpy.mean(x))

# + {"slideshow": {"slide_type": "-"}, "tags": []}
iris.iloc[:, 0:4].aggregate(lambda x: [numpy.mean(x), numpy.std(x)])

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Descriptive statistics for the iris dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Use `iris.aggregate()` to apply one or more custom functions

# + {"slideshow": {"slide_type": "-"}, "tags": []}
iris.iloc[:, 0:4].aggregate(['sum', 'mean', numpy.median])

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Descriptive statistics for the iris dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Use `iris.aggregate()` to compute range (max - min)

# + {"slideshow": {"slide_type": "-"}, "tags": []}
iris.iloc[:, 0:4].aggregate(lambda x: [numpy.max(x) - numpy.min(x)])

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Use `iris.aggregate()` to compute interquantile range (75th percentile - 25th percentile)

# + {"slideshow": {"slide_type": "-"}, "tags": []}
iris.iloc[:, 0:4].aggregate(lambda x: [numpy.quantile(x, 0.75) - numpy.quantile(x, 0.25)])

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Descriptive statistics for the iris dataset

# + {"slideshow": {"slide_type": "-"}, "tags": []}
iris.iloc[:, 0:4].aggregate(lambda x: numpy.quantile(x, [0.025, 0.25, 0.5, 0.75, 0.975]))

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Descriptive statistics for the iris dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Use `iris.describe()` to get summary statistics for the measurements

# + {"slideshow": {"slide_type": "-"}, "tags": []}
iris.describe()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Descriptive statistics for the iris dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Use `iris.describe(include='all')` to get summary statistics for all the columns

# + {"slideshow": {"slide_type": "-"}, "tags": []}
iris.describe(include='all')

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Descriptive statistics for the iris dataset

# + [markdown] {"slideshow": {"slide_type": "-"}}
# Use `iris.value_counts()` to get a breakdown of each species in the dataset

# + {"slideshow": {"slide_type": "-"}}
iris.species.value_counts()

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# Show results as fraction

# + {"slideshow": {"slide_type": "-"}}
iris.species.value_counts(normalize=True)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Descriptive statistics for the iris dataset by flower species

# + {"tags": [], "slideshow": {"slide_type": "-"}}
iris.groupby('species').describe().style.apply(lambda x: ['background: lightblue' if x.name == '50%' else '' for i in x], axis=1)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Stats beyond pandas built-in functions
# -

# There are a few main general pourpose libraries for stats in python:
#     - **statsmodels**: https://www.statsmodels.org/stable/index.html
#     - **scipy**: https://www.scipy.org/
#     -...
#
# ![statsmodels](images/statsmodels_hybi_banner.png)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Simple example using statsmodels

# + {"slideshow": {"slide_type": "-"}}
import statsmodels.formula.api
import seaborn


seaborn.swarmplot(x='species', y='petal length (cm)', data=iris);

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Simple example using statsmodels
# -

statsmodels.formula.api.ols('Q("petal length (cm)") ~ species', data=iris).fit().summary()

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Using R packages from python with rpy2
# -

# Some statistical functions may be available in R, but not in python.
# For this purpose you can call R packages from python using rpy2 (https://rpy2.bitbucket.io/).

# +
# Import rpy2 modules
import rpy2.robjects
import rpy2.robjects.packages
import rpy2.robjects.pandas2ri

# Save the R 'stats' package as a python variable
stats = rpy2.robjects.packages.importr('stats')

# Convert pandas dataframes to R objects
with rpy2.robjects.conversion.localconverter(
    rpy2.robjects.default_converter + rpy2.robjects.pandas2ri.converter):
    # Run model in R
    model = stats.lm('petal_length ~ species',
                     data=iris.rename(columns={'petal length (cm)': 'petal_length'}))
    summary = stats.summary_lm(model)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Using R packages from python with rpy2

# +
# Extract the coefficients
coef = stats.coef(summary)

# Convert coefficients to python dataframe
with rpy2.robjects.conversion.localconverter(
    rpy2.robjects.default_converter + rpy2.robjects.pandas2ri.converter):
    coef = pandas.DataFrame(rpy2.robjects.conversion.rpy2py(coef), index=coef.rownames, columns=coef.colnames)

coef

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Practise
# 1. Go to http://tiny.cc/4c33gz
# 2. Click on "05_pandas_data_cleaning_and_preprocessing_practise.py"
# 3. Play around
#
# Continue in 10 min.
