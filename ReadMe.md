[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fbitbucket.org%2Fmanuela_s%2Fpython_workshop%2Fsrc%2Fmaster/master)

# Workshop
Presentation slides and materials for the workshop ["Python for data analysis and visualization (part 2)"](https://staff.rcsi.ie/upcoming-courses/research-data-science/advanced-python-workshop) held as part of the module "PC10 Optional: Fundamentals of Computational Biology, 2019-2020), (RCSI Dublin, 2019-12-04).

## Overview:
This is the second of two python workshops. This workshop focuses on importing, cleaning and plotting data. It is recommended to attend the “Introduction to Python programming” workshop first for attendees with no previous Python experience.

## Learning outcomes:
By the end of this session, you will be able to:

 - Install and get started with Python and PyCharm (editor for Python code development)
 - Read in data-files from csv-files, text-files, spreadsheets, databases or with pandas
 - Perform simple data processing (basic data-cleaning, aggregations, ..) with pandas
 - Do basic statistics in python (statsmodels) or using R packages from python
 - Make  publication-quality plots with matplotlib and seaborn
 - Bring it all together with a hand-on example

## Target Audience:
Vitae Researcher Development Framework: Domain A (Knowledge and Intellectual Abilities)

## Materials
- Presentation slides:
    - [html format](https://manuelas.bitbucket.io/talk/pythonworkshop/presentation.slides.html)
    - [pdf format](https://manuelas.bitbucket.io/talk/pythonworkshop/presentation_slide.pdf)
- Hands-on examples
    - [binder](https://mybinder.org/v2/git/https%3A%2F%2Fbitbucket.org%2Fmanuela_s%2Fpython_workshop%2Fsrc%2Fmaster/master)

## Contact
For further information, please get in touch: manuelasalvucci@rcsi.ie
