# ---
# jupyter:
#   jupytext:
#     cell_metadata_json: true
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Jupyter notebook foundamental - Section 2

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # What's a jupyter notebook?
#
# ![jupyter_notebook_logo](images/jupyter_notebook_logo.png)
#
# - "jupyter" + "notebook"
#
#     - "jupyter": "acronym" from Julia + python + R (though many more languages are now supported)
#     - "notebook": collection of text, code, plots for an analysis (literate programming)
# - Jupyter project: https://jupyter.org/index.html
#
# - Gallery of interesting jupyter notebooks: https://github.com/jupyter/jupyter/wiki/A-gallery-of-interesting-Jupyter-Notebooks

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Anatomy of a jupyter notebook
# -

# * Notebook consists of a series of "cells"
# * Each cell can have text (markdown) or code (in our case, python).
# * A cell can be run by selecting it and pressing ![Run](images/jupyter_notebook_run_icon.png) button at the top (or pressing ctr-enter)
# * When the cell is run, the output is shown underneath: ![Cell](images/jupyter_notebook_cell_output.png)

# - Simple expressions

# + {"slideshow": {"slide_type": "-"}}
2+2

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# - Comments

# +
# Lines that start with a # are comments.

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# - Store data in variables

# + {"slideshow": {"slide_type": "-"}}
a = 2 + 2
b = 1 + 1
a + b

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# - Cells can reference variables from prevoius cells (make sure to run the previous cells first!):

# + {"slideshow": {"slide_type": "-"}}
a - b

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Where can I find this material?
# - Presentation and code materials at https://bitbucket.org/manuela_s/python_workshop/
# - Binder version at http://tiny.cc/4c33gz
# - Any question, comment, come chat to me or email me at manuelasalvucci@rcsi.ie
#
# # Practise
# 1. Go to http://tiny.cc/4c33gz
# 2. Click on "02_jupyter_notebook_practise.py"
# 3. Play around
#
# Continue in 5 min.
