# ---
# jupyter:
#   jupytext:
#     cell_metadata_json: true
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.0
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Python for data analysis and visualization (part 2)
#
# ### Manuela Salvucci, RCSI
# ### manuelasalvucci@rcsi.ie
# #### 2019-12-04

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Agenda
#
# 1. Getting Started
# 2. Jupyter Notebook
# 3. Pandas imports
# 4. Pandas dataframes
# 5. Pandas data cleaning and preprocessing
# 6. Pandas descriptive statistics
# 7. Data plotting
# 8. Full example
#
# Mix of theory and hands-on practise
#
# **Please go to http://tiny.cc/4c33gz**

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Agenda
#
# Please go to http://tiny.cc/4c33gz
#
# ![binder](images/binder.png)

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Getting started - section 1
#
# Expectations: "Introduction to Python programming" or similar basic python experience.

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Installing software

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * **python** - https://www.python.org/downloads/

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * **anaconda**: includes python and all the data-science libraries used in this workshop (numpy, pandas, matplotlib, seaborn) - https://www.anaconda.com/distribution

# + [markdown] {"slideshow": {"slide_type": "fragment"}}
# * **pycharm**: IDE for python development -  https://www.jetbrains.com/pycharm/download

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Pycharm
#
# - Integrated Development Environment for python
# - Simplifies writing and running python code
# - Available for Windows, Mac and Linux
# - Available in Professional, Community and Educational edition. Community (free) editon has everything you need.

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Pycharm
#
# <img src="images/pycharm_features.png">

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # Pycharm
#
# <img src="images/simpleLook@2x.jpg">

# + [markdown] {"slideshow": {"slide_type": "slide"}}
# # But not today...
#
# Will use online version of jupyter notebook, so no local installation required.
